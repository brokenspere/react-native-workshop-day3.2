import React, { Component } from 'react'
import { View ,StyleSheet,Text} from 'react-native'
import { connect } from 'react-redux'
import { Button } from '@ant-design/react-native'


class profile extends Component {
    goEditProfile = () => {
        this.props.history.push('/EditProfile')


    }

    render() {
        return(

        
        <View style={styles.container}>
            <View style={{ backgroundColor: '#BDB76B', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                <Text style={{ color: 'white', fontWeight: '500' }}>Profile</Text>


            </View>

            <View style={{ flex: 1, backgroundColor: 'white' }}>

            </View>
            <View style={{ flex: 2, backgroundColor: 'white' }}>

                <View style={[styles.column, styles.center]}>
                     <Text>Firstname</Text>
                     <Text>{this.props.user[0].firstname}</Text>
                </View>
                <View style={[styles.column, styles.center]}>
                     <Text>Firstname</Text>
                     <Text>{this.props.user[0].lastname}</Text>
                </View>
                <View style={[styles.column, styles.center]}>
                     <Text>Username</Text>
                     <Text>{this.props.user[0].username}</Text>
                </View>

            </View>
            <View style={{ flex: 1, backgroundColor: 'white' }}>

            </View>

            <View style={{ backgroundColor: '#BDB76B' }}         >
                <View style={styles.column}>
                    <View style={styles.button}>
                        <Button  onPress={this.goEditProfile}>Edit</Button>
                    </View>

                </View>

            </View>

        </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'blue',
        flex: 1
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'

    },
    center: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 8
    },
    button: {
        margin: 6,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'


    },
    circle: {

        width: 100,
        height: 100,
        borderRadius: 100,
        margin: 24,
        alignItems: 'center',
    },
    column: {
        flexDirection: 'column'
    },
    textInput: {
        backgroundColor: 'white',
        margin: 10,
        width: 250,
        borderRadius: 100

    }
})
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, null)(profile)