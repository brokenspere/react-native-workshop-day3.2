

import {createStore ,combineReducers,applyMiddleware,compose} from 'redux'
import logger from 'redux-logger'
import {routerMiddleware,connectRouter} from 'connected-react-router'
import ProductReducer from './productReducer'
import UserReducer from './userReducer';

import {createMemoryHistory} from 'history'




const reducer = (history)=>combineReducers({
    user: UserReducer,
    product: ProductReducer,
    router: connectRouter(history)
})

export const history = createMemoryHistory()

export const store = createStore(
    reducer(history),
    compose(
        applyMiddleware(
            routerMiddleware(history),
            logger
        )
    )
)
