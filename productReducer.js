export default (state =[] ,action)=>{
    switch(action.type){
        case 'ADD_PRODUCT':
            return  [...state,{
                name: action.name,
                image: action.image
            }]
        
         default:
            return state      
    }
}