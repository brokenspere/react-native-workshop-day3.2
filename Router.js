import React , {Component} from 'react'
import {Route,Switch,Redirect} from 'react-router-native'
import {ConnectedRouter} from 'connected-react-router'
import { Provider} from 'react-redux'
import {store , history} from './AppStore'


import Login from './LoginPage'
import Main from  './Main'
import Profile from './Profile'
import AddProduct from './AddProduct'
import ProductDetail from './productDetail'
import EditProduct from './editProduct'
import EditProfile from './EditProfile'

class Router extends Component{

  

    render(){
        return(
        <Provider store={store}>
             <ConnectedRouter history = {history}>
              <Switch>
                    <Route exact path="/" component={Login}/>
                    <Route exact path="/Main" component={Main}/>
                    <Route exact path="/Profile" component={Profile}/>
                    <Route exact path="/Add" component={AddProduct}/>
                    <Route exact path="/ProductDetail"component={ProductDetail}/>
                    <Route exact path="/Edit"component={EditProduct}/>
                    <Route exact path="/EditProfile"component={EditProfile}/>
                    <Redirect to="/"/>
              </Switch>
          </ConnectedRouter>
        </Provider>
         
        )
    }
}

export default Router