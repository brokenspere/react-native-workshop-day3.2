import React, { Component } from 'react'
import { View, Text,StyleSheet ,Alert} from 'react-native'
import { connect } from 'react-redux'
import { Button, InputItem, List } from '@ant-design/react-native';

class Editprofile extends Component {
    state = {
        firstname: '',
        lastname: '',
        
    }
    
    
    saveEditProfile=()=>{
        this.props.editUser(this.props.user[0].username,this.state.firstname,this.state.lastname)
        Alert.alert('Edit success')
        this.props.history.push('/Profile')
    }
    
    render() {
        const {editUser}= this.props.user[0]
        return (
            <View style={styles.container}>
                <View style={{ backgroundColor: '#BDB76B', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                    <Text style={{ color: 'white', fontWeight: '500' }}>Edit Profile</Text>


                </View>
                <View style={{ flex:1, backgroundColor: 'white' }}>
                
                </View>
                <View style={{ flex: 2, backgroundColor: 'white' }}>

                    
                    <View style={[styles.column, styles.center]}>
                        <InputItem placeholder="new fistname"onChangeText={(firstname) => this.setState({ firstname })}>firstname</InputItem>
                    </View>
                    <View style={[styles.column, styles.center]}>
                        <InputItem placeholder="new lastname" onChangeText={(lastname) => this.setState({ lastname })}>lastname</InputItem>
                    </View>
                    






                </View>
                <View style={{ flex:1, backgroundColor: 'white' }}>
                
                </View>

                <View style={{ backgroundColor: '#BDB76B' }}         >
                    <View style={styles.column}>
                        <View >
                            <Button  onPress={this.saveEditProfile}>Save</Button>
                        </View>

                    </View>

                </View>

            </View>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'blue',
        flex: 1
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'

    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        margin: 6,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'


    },
    circle: {

        width: 100,
        height: 100,
        borderRadius: 100,
        margin: 24,
        alignItems: 'center',
    },
    column: {
        flexDirection: 'column'
    },
    textInput: {
        backgroundColor: 'white',
        margin:10,
        width: 250,
        borderRadius: 100

    }
})
const mapStateToProps = (state) => {
    return {
        user: state.user

    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        editUser:(username,firstname,lastname) =>{
            console.log('username', username)
            console.log('firstname', firstname)
            console.log('lastname', lastname)
            dispatch({
                type: 'EDIT_USER',    
                username:username,            
                firstname:firstname,
                lastname:lastname
            })
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Editprofile)