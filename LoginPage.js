import React, { Component } from 'react'
import {  StyleSheet, Text, View, TextInput, Alert, Image } from 'react-native';
import {Button,InputItem} from '@ant-design/react-native'
import {connect} from 'react-redux'



class logIn extends Component {
    state={
        username:'',
        password:''
    }
    UNSAFE_componentWillMount(){
        this.props.addUser('admin','admin')
    }

    goMainPage=()=>{
        if(this.props.user[0].username && this.props.user[0].password){
            this.props.history.push('/Main')
        }else{
            Alert.alert('plz input your username and password')
        }
    }

    render() {
        return (

            <View style={styles.container}>
                <View style={{ flex: 1, backgroundColor: 'gray' }}>
                    <View style={[styles.row,styles.center]}>
                        
                        <Image source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwhDm6lexYN9KzipKXhbx9PEzFIZ4poeI6yuKZfh23b2w4A983' }}
                             style={styles.circle} />
                    
                    </View>

                </View>

                <View style={{ flex: 1, backgroundColor: 'gray' }}>
                    <View style={[styles.row, styles.center]}>
                        <TextInput style={styles.textInput}

                            placeholder="Username"
                            onChangeText={(username) => this.setState({ username })}

                        />
                    </View>

                    <View style={[styles.row, styles.center]}>
                        <TextInput style={styles.textInput}

                            placeholder="password"
                            onChangeText={(password) => this.setState({ password })}

                        />


                    </View>

                    <View style={[styles.row, styles.center]}>


                        <Button
                            onPress={this.goMainPage}
                          type="primary"
                        >LOGIN</Button>



                    </View>

                </View>

            </View>

        )
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: 'blue',
        flex: 1
    },
    textInput: {
        backgroundColor: 'white',
        margin: 10,
        width: 250,
        borderRadius:100

    },
    row: {
        flexDirection: 'row',

    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    circle: {
  
        width:100,
        height: 100,
        borderRadius: 100,
        margin:100,
        alignItems: 'center',
      },




})

const mapStateToProps =(state)=>{
    return{
        user: state.user
    }
}

const mapDispatchToProps =(dispatch)=>{
    return {
        addUser :(username,password)=>{
             dispatch({
                 type: 'ADD_USER',
                 username: username,
                 password: password
             })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(logIn) 
    