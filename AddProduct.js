import React, { Component } from 'react'
import { StyleSheet, Text, View, Alert} from 'react-native';
import { Button ,InputItem } from '@ant-design/react-native'
import {connect} from 'react-redux'




class AddProduct extends Component{

    saveEditProduct = ()=>{
        Alert.alert('Add success')
        this.props.history.push('/Main')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ backgroundColor: '#BDB76B', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                    <Text style={{ color: 'white', fontWeight: '500' }}>Add new Product</Text>


                </View>

                <View style={{ flex:1, backgroundColor: 'white' }}>
                
                </View>
                <View style={{ flex: 2, backgroundColor: 'white' }}>

                    <View style={[styles.column, styles.center]}>
                        <InputItem  placeholder="image picker">image</InputItem>
                    </View>
                    <View style={[styles.column, styles.center]}>
                        <InputItem placeholder="product name">Name</InputItem>
                    </View>

                </View>
                <View style={{ flex:1, backgroundColor: 'white' }}>
                
                </View>

                <View style={{ backgroundColor: '#BDB76B' }}         >
                    <View style={styles.column}>
                        <View style={styles.button}>
                            <Button  onPress={this.saveEditProduct}>Save</Button>
                        </View>

                    </View>

                </View>

            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'blue',
        flex: 1
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'

    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        margin: 6,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'


    },
    circle: {

        width: 100,
        height: 100,
        borderRadius: 100,
        margin: 24,
        alignItems: 'center',
    },
    column: {
        flexDirection: 'column'
    },
    textInput: {
        backgroundColor: 'white',
        margin:10,
        width: 250,
        borderRadius: 100

    }
})


const mapDispatchToProps = (dispatch)=>{
    return{
        addProduct:(name,image)=>{
            dispatch({
                type:'ADD_PRODUCT',
                name:name,
                image:image
            })
        }
    }
}


export default connect(null,mapDispatchToProps)(AddProduct)