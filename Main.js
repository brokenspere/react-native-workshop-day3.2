import React, { Component } from 'react'
import { StyleSheet, Text, View, TextInput, Alert, Image, ScrollView, TouchableOpacity } from 'react-native';
import { Button ,List } from '@ant-design/react-native'
import {connect} from 'react-redux'

const Item = List.Item;

class Main extends Component {

    goProfilePage = () => {
        this.props.history.push('/Profile')
    }
    addNewProduct = () => {
        this.props.history.push('/Add')
    }
    clickProduct = () => {
        this.props.history.push('/ProductDetail')
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ backgroundColor: '#BDB76B', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                    <Text style={{ color: 'white', fontWeight: '500' }}>Product</Text>
                   

                </View>

                <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
                   <List>
                   <Item onPress={this.clickProduct} thumb="https://d2t5tgzzxhb6v2.cloudfront.net/media/catalog/category/Fresh-Fruits.png"> แอปเปิ้ล</Item>
                   </List>
                   <List>
                   <Item onPress={this.clickProduct} thumb="https://sites.google.com/site/fruittongpraisetl/_/rsrc/1440325733398/phl-mi-tam-vdukal-1/Durean.png"> ทุเรียน</Item>
                   </List>
                   <List>
                   <Item onPress={this.clickProduct} thumb="https://d2t5tgzzxhb6v2.cloudfront.net/media/catalog/category/water-melon-2-0218507000007-icon.png"> แตงโม</Item>
                   </List>
                   <List>
                   <Item onPress={this.clickProduct} thumb="https://lifestyle.campus-star.com/app/uploads/2018/02/c-banana.jpg"> กล้วย</Item>
                   </List>
                   <List>
                   <Item onPress={this.clickProduct} thumb="http://postcoloniality.org/wp-content/uploads/2017/07/fru13.jpg"> ส้ม</Item>
                   </List>
                   <List>
                   <Item onPress={this.clickProduct} thumb="https://d2t5tgzzxhb6v2.cloudfront.net/media/catalog/category/Apple-_-Pear.png"> แพร์</Item>
                   </List>
                   <List>
                   <Item onPress={this.clickProduct} thumb="http://health.mthai.com/app/uploads/2017/03/monkey-apple.jpg"> อะหยังอะ</Item>
                   </List>
                   <List>
                   <Item onPress={this.clickProduct} thumb="http://www.oknation.net/blog/home/user_data/file_data/201312/14/6693496e1.jpg"> มะเฟือง</Item>
                   </List>
                   <List>
                   <Item onPress={this.clickProduct} thumb="https://i.ytimg.com/vi/6UIwMPWBdug/maxresdefault.jpg"> มะละกอ</Item>
                   </List>
                   <List>
                   <Item onPress={this.clickProduct} thumb="https://undubzapp.com/wp-content/uploads/2016/03/08.jpg"> เสาวรส</Item>
                   </List>
                   <List>
                   <Item onPress={this.clickProduct} thumb="https://f.ptcdn.info/518/041/000/o53r0r83kuqDeHE5rx4-o.jpg"> มะเขือเทศ</Item>
                   </List>
                   <List>
                   <Item onPress={this.clickProduct} thumb="http://www.oknation.net/blog/home/blog_data/528/2528/images/page006.jpg"> องุ่น</Item>
                   </List>

                </ScrollView>

                <View style={{ backgroundColor: '#BDB76B' }}         >
                    <View style={styles.column}>
                        <View style={styles.button}>
                            <Button  onPress={this.addNewProduct}>Add new product</Button>
                            <Button  onPress={this.goProfilePage}>Profile</Button>
                        </View>

                    </View>

                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: 'blue',
        flex: 1
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'

    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        margin: 6,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'


    },
    circle: {

        width: 100,
        height: 100,
        borderRadius: 100,
        margin: 24,
        alignItems: 'center',
    },
    column: {
        flexDirection: 'column'
    }
})

const mapStateToProps =(state)=>{
    return{
        product: state.product
    }
}

export default connect(mapStateToProps,null)(Main)