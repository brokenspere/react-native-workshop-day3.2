import React, { Component } from 'react'
import { StyleSheet, Text, View, TextInput} from 'react-native';
import { Button } from '@ant-design/react-native'
import {connect} from 'react-redux'



class ProductDetail extends Component {
    goEditProduct =()=>{
        this.props.history.push('/Edit')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ backgroundColor: '#BDB76B', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                    <Text style={{ color: 'white', fontWeight: '500' }}>Product Detail</Text>


                </View>

                <View style={{ flex: 1, backgroundColor: 'white' }}>

                    <View style={[styles.column,styles.center]}>
                        <Text style={styles.textInput}>Image</Text>
                        <Text style={styles.textInput}>Name</Text>
                    </View>





                </View>

                <View style={{ backgroundColor: '#BDB76B' }}         >
                    <View style={styles.column}>
                        <View style={styles.button}>
                            <Button  onPress={this.goEditProduct}>Edit</Button>
                        </View>

                    </View>

                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: 'blue',
        flex: 1
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'

    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        margin: 6,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'


    },
    circle: {

        width: 100,
        height: 100,
        borderRadius: 100,
        margin: 24,
        alignItems: 'center',
    },
    column: {
        flexDirection: 'column'
    },
    textInput: {
      
        margin:10,
        width: 250,
        borderRadius: 100

    }
})

const mapStateToProps =(state)=>{
    return{
        product: state.product
    }
}

export default connect(mapStateToProps,null)(ProductDetail)